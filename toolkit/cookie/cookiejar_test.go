package cookie

import "testing"

func TestNewCookiejar(t *testing.T) {
	cj := NewCookiejar()
	if cj == nil {
		t.Fatal("Couldn't create cookiejar")
	}
}

func TestMyPublicSuffixList(t *testing.T) {
	domains := []string{
		"golang.org",
		"cn.bing.com",
		"zhihu.sougou.com",
		"www.beijing.gov.cn",
	}
	exceptedPSs := []string{
		"org",
		"com",
		"com",
		"gov.cn",
	}
	psl := &myPublicSuffixList{}
	for i, domain := range domains {
		eps := exceptedPSs[i]
		suffix := psl.PublicSuffix(domain)
		if eps != suffix {
			t.Fatalf("Inconsistent publice suffix for domain %q: expected: %s, actual: %s",
				domain, eps, suffix)
		}
	}
	eptString := "Web crawler - public suffix list (rev 1.0) power by \"golang.org/x/net/publicsuffix\""
	if psl.String() != eptString {
		t.Fatalf("Inconsistent string for myPublicSuffixList: expected: %s, actual: %s",
			eptString, psl.String())
	}
}
