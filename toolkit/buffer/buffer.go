/*
	缓冲器, chan作为数据的传递方式, 缓冲区有读写操作， ⚠注意读写锁
*/
package buffer

import (
	"Gspider/errors"
	"fmt"
	"sync"
	"sync/atomic"
)

type Buffer interface {
	Cap() uint32
	Len() uint32
	Put(data interface{}) (bool, error)
	Get() (interface{}, error)
	Close() bool
	Closed() bool
}

type myBuffer struct {
	ch chan interface{}
	closed uint32
	closingLock sync.RWMutex
}


func NewBuffer(size uint32) (Buffer, error) {
	if size == 0 {
		errMsg := fmt.Sprintf("Illegal size for buffer: %d", size)
		return nil, errors.NewIllegalParameterError(errMsg)
	}
	return &myBuffer{
		ch: make(chan interface{}, size),
	}, nil
}

func (mb *myBuffer) Cap() uint32 {
	return uint32(cap(mb.ch))
}

func (mb *myBuffer) Len() uint32 {
	return uint32(len(mb.ch))
}

func (mb *myBuffer) Put(data interface{}) (ok bool, err error) {
	mb.closingLock.RLock()
	defer mb.closingLock.RUnlock()
	if mb.Closed() {
		return false, ErrClosedBuffer
	}
	select {
	case mb.ch <- data:
		ok = true
	default:
		ok = false
	}
	return
}

func (mb *myBuffer) Get() (interface{}, error) {
	select {
	case data, ok := <- mb.ch:
		if !ok {
			return nil, ErrClosedBuffer
		}
		return data, nil
	default:
		return nil, nil
	}
}

func (mb *myBuffer) Close() bool {
	if atomic.CompareAndSwapUint32(&mb.closed, 0, 1) {
		mb.closingLock.Lock()
		close(mb.ch)
		mb.closingLock.Unlock()
		return true
	}
	return false
}

func (mb *myBuffer) Closed() bool {
	if atomic.LoadUint32(&mb.closed) == 0 {
		return false
	}
	return true
}