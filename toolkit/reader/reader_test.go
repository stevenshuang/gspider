package reader

import (
	"bytes"
	"io"
	"strings"
	"testing"
)

func TestNewMultiReader(t *testing.T) {
	eptData := "0987dcba"
	rr, err := NewMultiReader(strings.NewReader(eptData))
	if err != nil {
		t.Fatalf("An error occurs when new multi reader: %s", err)
	}
	buffer := new(bytes.Buffer)
	_, err = io.Copy(buffer, rr.Reader())
	if err != nil {
		t.Fatalf("An error occur when copying data: %s", err)
	}
	content1 := buffer.String()
	if content1 != eptData {
		t.Fatalf("Wrong data: %s, excepted: %s",
			content1, eptData)
	}
	content2 := buffer.String()
	if content2 != eptData {
		t.Fatalf("Wrong data: %s, excepted: %s",
			content2, eptData)
	}
}
