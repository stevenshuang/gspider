package reader

import (
	"bytes"
	"io"
	"io/ioutil"
)

type MultiReader interface {
	Reader() io.ReadCloser
}

type myMultiReader struct {
	data []byte
}

func NewMultiReader(reader io.Reader) (MultiReader, error) {
	var data []byte
	var err error
	if reader != nil {
		data, err = ioutil.ReadAll(reader)
		if err != nil {
			return nil, err
		}
	} else {
		data = []byte{}
	}
	return &myMultiReader{
		data: data,
	}, nil
}

func (mmr *myMultiReader) Reader() io.ReadCloser {
	// 么有关闭的读
	return ioutil.NopCloser(bytes.NewReader(mmr.data))
}