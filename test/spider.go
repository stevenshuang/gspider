package test

import (
	"Gspider/scheduler"
	"flag"
	"fmt"
	"strings"
)


var domains string
func init() {
	flag.StringVar(&domains, "domains", "acm.hdu.edu.cn", "")
}

func Spider() (error){
	sched := scheduler.NewScheduler()
	domainParts := strings.Split(domains, ",")
	acceptDomains := []string{}
	for _, domain := range domainParts {
		domain = strings.TrimSpace(domain)
		if domain != "" {
			acceptDomains = append(acceptDomains, domain)
		}
	}
	reqArgs := scheduler.RequestArgs{
		AcceptedDomains: acceptDomains,
		MaxDepth: uint32(1),
	}
	dataArgs := scheduler.DataArgs{
		ReqBufferCap: 200,
		ReqMaxBufferNumber: 1000,
		RespBufferCap: 200,
		RespMaxBufferNumber: 100,
		ItemBufferCap: 200,
		ItemMaxBufferNumber: 50,
		ErrorMaxBufferNumber: 1,
		ErrorBufferCap: 50,
	}
	downloaders, err := GenDownloader(10)
	if err != nil {
		fmt.Println("downloader error")
		return err
	}
	analyzers, err := GenAnalyzer(10)
	if err != nil {
		fmt.Println("analyzers error")
		return err
	}
	pipelines, err := GenPipeline(10)
	if err != nil {
		fmt.Println("pipelines error")
		return err
	}
	moduleArgs := scheduler.ModuleArgs{
		Downloaders: downloaders,
		Analyzers: analyzers,
		Pipelines: pipelines,
	}
	err = sched.Init(reqArgs, dataArgs, moduleArgs)
	if err != nil {
		fmt.Println("scheduler init error")
		return err
	}
	reqList, err := Gen(1099)
	if err != nil {
		fmt.Println("gen req list error")
		return err
	}
	err = sched.StartWithReqList(reqList)
	if err != nil {
		fmt.Println("start with reqList  error")
		return err
	}
	return nil
}