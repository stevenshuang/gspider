package test

import (
	"errors"
	"fmt"
)

func genProblemIdError(problemId uint32) error {
	return errors.New(fmt.Sprintf(
		"Problem Id must >= 1000 <= 6460, get: %d", problemId))
}
