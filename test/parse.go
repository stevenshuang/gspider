/*
	生成测试函数
*/

package test

import (
	"Gspider/module"
	"errors"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"net/http"
	"strings"
)

const (
	baseUrlTemplate string = "http://acm.hdu.edu.cn/showproblem.php?pid=%d"
	desSelector = "body > table > tbody > tr:nth-child(4) > td > div:nth-child(6)"
	titleSelector = "body > table > tbody > tr:nth-child(4) > td > h1"
	inputSelector = "body > table > tbody > tr:nth-child(4) > td > div:nth-child(10)"
	outputSelector = "body > table > tbody > tr:nth-child(4) > td > div:nth-child(14)"
	sampleInputSelector = "body > table > tbody > tr:nth-child(4) > td > div:nth-child(18) > pre > div"
	sampleOutputSelector = "body > table > tbody > tr:nth-child(4) > td > div:nth-child(22) > pre > div"
)

// 先向respBufferPool写数据呀
func Gen(maxProblemId uint32) ([]*http.Request, error){
	if maxProblemId < 1000 || maxProblemId > 6461 {
		return nil, genProblemIdError(maxProblemId)
	}
	res := make([]*http.Request, 0)
	for i := uint32(1000); i <= maxProblemId; i ++ {
		url := fmt.Sprintf(baseUrlTemplate, i)
		req, err := http.NewRequest("GET", url, nil)
		if err == nil {
			res = append(res, req)
		} else {
			return nil, errors.New("an http request error, stop to generate")
		}
	}
	return res, nil
}

func GenParseResp() []module.ParseResponse {
	parseContent := func(resp *http.Response, dep uint32) ([]module.Data, []error) {
		dataList := make([]module.Data, 0)
		if resp == nil {
			return nil, []error{fmt.Errorf("nil http response")}
		}
		if resp.StatusCode != 200 {
			return nil, []error{fmt.Errorf("erro status code")}
		}
		if resp.Body == nil {
			return nil, []error{fmt.Errorf("empty body")}
		}
		reqUlr := resp.Request.URL.String()
		doc, err := goquery.NewDocumentFromReader(resp.Body)
		if err != nil {
			return dataList, []error{fmt.Errorf("can not read response body data")}
		}
		item := make(map[string]interface{})
		item["problemId"] = strings.Split(reqUlr, "=")[1]
		item["problemTitle"] = doc.Find(titleSelector).Text()
		item["problemContent"] = doc.Find(desSelector).Text()
		item["problemInput"] = doc.Find(inputSelector).Text()
		item["problemOutput"] = doc.Find(outputSelector).Text()
		item["problemSampleInput"] = doc.Find(sampleInputSelector).Text()
		item["problemSampleOutput"] = doc.Find(sampleOutputSelector).Text()
		dataList = append(dataList, module.Item(item))
		return dataList, nil
	}
	return []module.ParseResponse{parseContent}
}

func genItemProcessors() []module.ProcessItem {
	// you can also define an database connect, data into database
	printItem := func(item module.Item) (result module.Item, err error) {
		for k, v := range item {
			fmt.Printf("[%s] --> %s\n", k, v)
		}
		return nil, nil
	}
	return []module.ProcessItem{printItem}
}