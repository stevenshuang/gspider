package test

import (
	"Gspider/module"
	"Gspider/module/local/analyzer"
	"Gspider/module/local/downloader"
	"Gspider/module/local/pipeline"
	"net"
	"net/http"
	"time"
)

func genHTTPClient() *http.Client {
	return &http.Client{
		Transport: &http.Transport{
			Proxy: http.ProxyFromEnvironment,
			DialContext: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
				DualStack: true,
			}).DialContext,
			MaxIdleConns:          100,
			MaxIdleConnsPerHost:   5,
			IdleConnTimeout:       60 * time.Second,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
		},
	}
}


var sn = module.NewSNGemerator(1, 0)
func GenDownloader(num uint8) ([]module.Downloader, error) {
	downloaders := []module.Downloader{}
	if num == 0 {
		return downloaders, nil
	}
	for i := uint8(0); i < num; i ++ {
		mid, err := module.GenMID(module.TYPE_DOWNLOADER,
			sn.Get(), nil)
		if err != nil {
			return downloaders, err
		}
		d, err := downloader.New(mid, genHTTPClient(), module.CalculateScoreSimple)
		if err != nil {
			return downloaders, err
		}
		downloaders = append(downloaders, d)
	}
	return downloaders, nil
}

func GenAnalyzer(num uint8) ([]module.Analyzer, error) {
	analyzers := []module.Analyzer{}
	if num == 0 {
		return analyzers, nil
	}
	for i := uint8(0); i < num; i ++ {
		mid, err := module.GenMID(module.TYPE_ANALYZER, sn.Get(), nil)
		if err != nil {
			return analyzers, err
		}
		a, err := analyzer.New(mid, GenParseResp(), module.CalculateScoreSimple)
		if err != nil {
			return analyzers, err
		}
		analyzers = append(analyzers, a)
	}
	return analyzers, nil
}

func GenPipeline(num uint8) ([]module.Pipeline,error) {
	pipelines := []module.Pipeline{}
	if num == 0 {
		return pipelines, nil
	}
	for i := uint8(0); i < num; i ++ {
		mid, err := module.GenMID(module.TYPE_PIPELINE, sn.Get(),
			nil)
		if err != nil {
			return pipelines, err
		}
		p, err := pipeline.New(mid, genItemProcessors(), module.CalculateScoreSimple)
		if err != nil {
			return pipelines, err
		}
		pipelines = append(pipelines, p)
	}
	return pipelines, nil
}