package test

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"net/http"
	"strings"
	"testing"
)

var baseUrl = "http://acm.hdu.edu.cn/showproblem.php?pid=1000"
const selector = "body > table > tbody > tr:nth-child(4) > td > h1"


func TestQuery(t *testing.T) {
	resp, err := http.Get(baseUrl)
	if err != nil {
		fmt.Println("resp error")
	}
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	problemId := strings.Split(baseUrl, "=")[1]
	problemTitle := doc.Find(selector).Text()
	fmt.Println(problemTitle, problemId)
}