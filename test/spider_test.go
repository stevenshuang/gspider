package test

import (
	"fmt"
	"testing"
)

func TestSpider(t *testing.T) {
	err := Spider()
	if err == nil {
		fmt.Println("accepted", err)
	} else {
		fmt.Println("failed", err)
	}
}
