/*
	define ErrorType
*/
package errors

import (
	"bytes"
	"fmt"
	"strings"
)

type ErrorType string

const (
	ERROR_TYPE_DOWNLOADER ErrorType = "downloader error"
	ERROR_TYPE_ANALYZER ErrorType = "analyzer error"
	ERROR_TYPE_PIPELINE ErrorType = "pipeline error"
	ERROR_TYPE_SCHEDULER ErrorType = "scheduler error"
)

type CrawlerError interface {
	Type() ErrorType
	Error() string
}

type myCrawlerError struct {
	errType ErrorType
	errMsg string
	fullErrMsg string
}

func NewCrawlerError(errType ErrorType, errMsg string) CrawlerError {
	return &myCrawlerError{
		errType: errType,
		errMsg: strings.TrimSpace(errMsg),
	}
}

func NewCrawlerErrorBy(errorType ErrorType, err error) CrawlerError {
	return NewCrawlerError(errorType, err.Error())
}

func (mcl *myCrawlerError) Type() ErrorType {
	return mcl.errType
}

func (mcl *myCrawlerError) Error() string {
	if mcl.fullErrMsg == "" {
		mcl.genFullErrMsg()
	}
	return mcl.fullErrMsg
}

func (mcl *myCrawlerError) genFullErrMsg() {
	var buf bytes.Buffer
	buf.WriteString("crawler error: ")
	if mcl.errType != "" {
		buf.WriteString(string(mcl.errType))
		buf.WriteString(": ")
	}
	buf.WriteString(mcl.errMsg)
	mcl.fullErrMsg = buf.String()
	return
}


type IllegalParameterError struct {
	msg string
}

func NewIllegalParameterError(msg string) IllegalParameterError {
	return IllegalParameterError{
		msg: fmt.Sprintf("illegal parameter: %s",
			strings.TrimSpace(msg)),
	}
}

func (ipe IllegalParameterError) Error() string {
	return ipe.msg
}