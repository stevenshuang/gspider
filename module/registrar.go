/*
	define Register 用来注册Module(downloader, analyzer, pipeline), 共调度器使用
	register 是共有的，要使用锁机制, 避免在读的情况下， 写入
	根据module的MID, 然后是type进行注册
*/
package module

import (
	"Gspider/errors"
	"fmt"
	"sync"
)

type Registrar interface {
	Register(module Module) (bool, error)
	Unregister(mid MID) (bool, error)
	Get(moduleType Type) (Module, error)
	GetAllByType(moduleType Type) (map[MID]Module, error)
	GetAll() map[MID]Module
	Clear()
}


func NewRegistrar() Registrar {
	return &myRegistrar{
		moduleTypeMap: map[Type]map[MID]Module{},
	}
}

type myRegistrar struct {
	moduleTypeMap map[Type]map[MID]Module
	rwlock sync.RWMutex
}

func (mr *myRegistrar) Register(module Module) (bool, error) {
	if module == nil {
		return false, errors.NewIllegalParameterError("mil module instance")
	}
	mid := module.ID()
	parts, err := SplitMID(mid)
	if err != nil {
		return false, err
	}
	moduleType := legalLetterTypeMap[parts[0]]
	if !CheckType(moduleType, module) {
		errMsg := fmt.Sprintf("incorrect module type: %s", moduleType)
		return false, errors.NewIllegalParameterError(errMsg)
	}
	mr.rwlock.Lock()
	defer mr.rwlock.Unlock()
	modulds := mr.moduleTypeMap[moduleType]
	if modulds == nil {
		modulds = map[MID]Module{}
	}
	if _, ok := modulds[mid]; ok {
		return false, nil
	}
	// 新写入数据
	modulds[mid] = module
	mr.moduleTypeMap[moduleType] = modulds
	return true, nil
}

func (mr *myRegistrar) Unregister(mid MID) (bool, error) {
	parts, err := SplitMID(mid)
	if err != nil {
		return false, err
	}
	moduleType := legalLetterTypeMap[parts[0]]
	var deleted bool
	mr.rwlock.Lock()
	defer mr.rwlock.Unlock()
	if modules, ok := mr.moduleTypeMap[moduleType]; ok {
		if _, ok := modules[mid]; ok {
			delete(modules, mid)
			deleted = true
		}
	}
	return deleted, nil
}

func (mr *myRegistrar) Get(moduleType Type) (Module, error) {
	// 评分最低的？
	modules, err := mr.GetAllByType(moduleType)
	if err != nil {
		return nil, err
	}
	minScore := uint64(0)
	var selectModule Module
	for _, module := range modules {
		SetScore(module)
		if err != nil {
			return nil, err
		}
		score := module.Score()
		if minScore == 0 || score < minScore {
			selectModule = module
			minScore = score
		}
	}
	return selectModule, nil
}

func (mr *myRegistrar) GetAllByType(moduleType Type) (map[MID]Module, error) {
	if !LegalType(moduleType) {
		errMsg := fmt.Sprintf("illegal module type: %s", moduleType)
		return nil, errors.NewIllegalParameterError(errMsg)
	}
	mr.rwlock.RLock()
	defer mr.rwlock.RUnlock()
	modules := mr.moduleTypeMap[moduleType]
	if len(modules) == 0 {
		return nil, ErrNotFoundModuleInterface
	}
	result := map[MID]Module{}
	for mid, module := range modules {
		result[mid] = module
	}
	return result, nil
}

func (mr *myRegistrar) GetAll() map[MID]Module {
	res := map[MID]Module{}
	mr.rwlock.RLock()
	defer mr.rwlock.RUnlock()
	for _, modules := range mr.moduleTypeMap {
		for mid, module := range modules {
			res[mid] = module
		}
	}
	return res
}

func (mr *myRegistrar) Clear() {
	mr.rwlock.Lock()
	defer mr.rwlock.Unlock()
	mr.moduleTypeMap = map[Type]map[MID]Module{}
}