package module

import "errors"

var ErrNotFoundModuleInterface = errors.New("not found module interface")
