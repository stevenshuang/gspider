/*
	为module定义MID生成器, module的描述
*/
package module

import (
	"math"
	"sync"
)

type SNGenerator interface {
	// 设置最小的序列号
	Start() uint64
	// 获取最大的序列号
	Max() uint64
	// 获取下个序列号
	Next() uint64
	// 循环使用的次数
	CycleCount() uint64
	// 获得一个序列号， 并准备下一个
	Get() uint64
}

func NewSNGemerator(start uint64, max uint64) SNGenerator {
	if max == 0 {
		max = math.MaxUint64
	}
	return &myGenerator{
		start: start,
		max: max,
	}
}


type myGenerator struct {
	start uint64
	max uint64
	next uint64
	cycleCount uint64
	lock sync.RWMutex
}

func (mg *myGenerator) Start() uint64 {
	return mg.start
}

func (mg *myGenerator) Max() uint64 {
	return mg.max
}

func (mg *myGenerator) Next() uint64 {
	mg.lock.RUnlock()
	defer mg.lock.RUnlock()
	return mg.next
}

func (mg *myGenerator) CycleCount() uint64 {
	mg.lock.RLock()
	defer mg.lock.RUnlock()
	return mg.cycleCount
}

func (mg *myGenerator) Get() uint64 {
	mg.lock.Lock()
	defer mg.lock.Unlock()
	id := mg.next
	if id == mg.max {
		mg.next = mg.start
		mg.cycleCount ++
	} else {
		mg.next ++
	}
	return id
}