/*
	define Counts 一个计数器, 用于计算每个组件调用的次数
	define SummaryStruct 一个对结构的综合的描述(组件的调用次数， 完成情况)
	define Module interface 综合组件的属性，ID， Addr网络地址等属性， 后面用ModuleInterval做结构体的实例
	define Downloader, Analyzer(ParseResponse解析函数), Pipeline(ProcessItem处理item的函数)接口
*/
package module

import "net/http"

type Counts struct {
	CallCount uint64
	AcceptCount uint64
	CompleteCount uint64
	HandlingNumber uint64
}

// 对模块描述
type SummaryStruct struct {
	ID MID `json:"id"`
	Called uint64 `json:"called"`
	Accepted uint64 `json:"accepted"`
	Completed uint64 `json:"completed"`
	Handling uint64 `json:"handling"`
	Extra interface{} `json:"extra,omitempty"`
}

// 对模块的综合概述
type Module interface {
	ID() MID
	Addr() string
	Score() uint64
	SetScore(score uint64)
	ScoreCalculator() CalculateScore
	CalledCount() uint64
	AcceptedCount() uint64
	CompletedCount() uint64
	HandlingNumber() uint64
	Counts() Counts
	Summary() SummaryStruct
}

type Downloader interface {
	Module
	Download(req *Request) (resp *Response, err error)
}


type ParseResponse func(resp *http.Response, dep uint32) ([]Data, []error)
type Analyzer interface {
	Module
	RespParsers() []ParseResponse
	Analyze(resp *Response) ([]Data, []error)
}


type ProcessItem func(item Item) (result Item, err error)
type Pipeline interface {
	Module
	ItemProcessors() []ProcessItem
	Send(item Item) []error
	FailFast() bool
	SetFailFast(failFast bool)
}