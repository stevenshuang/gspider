/*
	对共有的接口描述， 直接内嵌对象使用方法提升，
	这也算是go的一种设计模式吧
*/

package stub

import "Gspider/module"

type ModuleInternal interface {
	module.Module
	IncrCalledCount()
	IncrAcceptedCount()
	IncrCompletedCount()
	IncrHandlingNumber()
	DecrHandlingNumber()
	Clear()
}
