/*
	对moduleInterval的实现， 为综合对module的接口实现, 直接使用方法提升
*/
package stub

import (
	"Gspider/errors"
	"Gspider/module"
	"fmt"
	"sync/atomic"
)

// Module 的属性
type myModule struct {
	mid module.MID
	addr string
	score uint64
	scoreCalculator module.CalculateScore
	calledCount uint64
	acceptedCount uint64
	completedCount uint64
	handlingNumber uint64
}

func NewModuleInternal(
	mid module.MID,
	scoreCalculator module.CalculateScore)(ModuleInternal, error) {
		parts, err := module.SplitMID(mid)
		if err != nil {
			return nil, errors.NewIllegalParameterError(
				fmt.Sprintf("illegal ID %q: %s", mid, err))
		}
		return &myModule{
			mid: mid,
			addr: parts[2],
			scoreCalculator: scoreCalculator,
		}, nil
}

func (mm *myModule) ID() module.MID {
	return mm.mid
}

func (mm *myModule) Addr() string {
	return mm.addr
}

func (mm *myModule) Score() uint64 {
	return atomic.LoadUint64(&mm.score)
}
func (mm *myModule) SetScore(score uint64) {
	atomic.StoreUint64(&mm.score, score)
}

func (mm *myModule) ScoreCalculator() module.CalculateScore {
	return mm.scoreCalculator
}

func (mm *myModule) CalledCount() uint64 {
	return atomic.LoadUint64(&mm.calledCount)
}

func (mm *myModule) AcceptedCount() uint64 {
	return atomic.LoadUint64(&mm.acceptedCount)
}

func (mm *myModule) CompletedCount() uint64 {
	count := atomic.LoadUint64(&mm.completedCount)
	return count
}

func (mm *myModule) HandlingNumber() uint64 {
	return atomic.LoadUint64(&mm.handlingNumber)
}

func (mm *myModule) Counts() module.Counts {
	return module.Counts{
		CallCount: atomic.LoadUint64(&mm.calledCount),
		AcceptCount: atomic.LoadUint64(&mm.acceptedCount),
		CompleteCount: atomic.LoadUint64(&mm.completedCount),
		HandlingNumber: atomic.LoadUint64(&mm.handlingNumber),
	}
}

func (mm *myModule) Summary() module.SummaryStruct {
	counts := mm.Counts()
	return module.SummaryStruct{
		ID: mm.ID(),
		Called: counts.CallCount,
		Accepted: counts.AcceptCount,
		Completed: counts.CompleteCount,
		Handling: counts.HandlingNumber,
		Extra: nil,
	}
}

func (mm *myModule) IncrCalledCount() {
	atomic.AddUint64(&mm.calledCount, 1)
}

func (mm *myModule) IncrAcceptedCount() {
	atomic.AddUint64(&mm.acceptedCount, 1)
}

func (mm *myModule) IncrCompletedCount() {
	atomic.AddUint64(&mm.completedCount, 1)
}

func (mm *myModule) IncrHandlingNumber() {
	atomic.AddUint64(&mm.handlingNumber, 1)
}

func (mm *myModule) DecrHandlingNumber() {
	atomic.AddUint64(&mm.handlingNumber, ^uint64(0))
}

func (mm *myModule) Clear() {
	atomic.StoreUint64(&mm.calledCount, 0)
	atomic.StoreUint64(&mm.acceptedCount, 0)
	atomic.StoreUint64(&mm.completedCount, 0)
	atomic.StoreUint64(&mm.handlingNumber, 0)
}