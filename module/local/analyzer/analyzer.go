/*
	定义Analyzer, 用于解析返回的页面， 把数据和错误返回
*/
package analyzer

import (
	"Gspider/module"
	"Gspider/module/stub"
	"Gspider/toolkit/reader"
	"fmt"
)

func New(
	mid module.MID,
	respParses []module.ParseResponse,
	scoreCalculator module.CalculateScore) (module.Analyzer, error) {
		moduleBase, err := stub.NewModuleInternal(mid, scoreCalculator)
		if err != nil {
			return nil, err
		}
		if respParses == nil {
			return nil, genParameterError("nil response parse")
		}
		if len(respParses) == 0 {
			return nil, genParameterError("empty response parse")
		}
		var innerParses []module.ParseResponse
		for i, parse := range respParses {
			if parse == nil {
				return nil, genParameterError(fmt.Sprintf("nil response parse[%d]", i))
			}
			innerParses = append(innerParses, parse)
		}
		return &myAnalyzer{
			ModuleInternal: moduleBase,
			respParsers: respParses,
		}, nil
}


type myAnalyzer struct {
	// 方法提升
	stub.ModuleInternal
	respParsers []module.ParseResponse
}

func (ma *myAnalyzer) Analyze(resp *module.Response) (dataList []module.Data, errorList []error) {
	ma.ModuleInternal.IncrHandlingNumber()
	defer ma.ModuleInternal.DecrHandlingNumber()
	ma.ModuleInternal.IncrCalledCount()
	if resp == nil {
		errorList = append(errorList, genParameterError("nil http response"))
		return
	}
	httpResp := resp.HTTPResp()
	if httpResp == nil {
		errorList = append(errorList, genParameterError("nil http response"))
	}
	var httpReq = httpResp.Request // 非自定义的Request
	if httpReq == nil {
		errorList = append(errorList, genParameterError("nil http request"))
	}
	var reqURL = httpReq.URL
	if reqURL == nil {
		errorList = append(errorList, genParameterError("nil http request url"))
	}
	// 验证完数据的正确性
	ma.ModuleInternal.IncrAcceptedCount()
	respDep := resp.Depth()
	if httpResp.Body != nil {
		defer httpResp.Body.Close()
	}
	multiReader, err := reader.NewMultiReader(httpResp.Body)
	if err != nil {
		errorList = append(errorList, genError(err.Error()))
		return
	}
	dataList = []module.Data{}
	for _, respParser := range ma.respParsers {
		httpResp.Body = multiReader.Reader()
		pDataList, pErrorList := respParser(httpResp, respDep)
		if pDataList != nil {
			for _, data := range pDataList {
				if pDataList == nil {
					continue
				}
				dataList = appendDataList(dataList, data, respDep)
			}
		}
		if pErrorList != nil {
			for _, pErr := range pErrorList {
				if pErr == nil {
					continue
				}
				errorList = append(errorList, pErr)
			}
		}
	}
	if len(errorList) == 0 {
		ma.ModuleInternal.IncrCompletedCount()
	}
	return dataList, errorList
}

func (ma *myAnalyzer) RespParsers() []module.ParseResponse {
		parsers := make([]module.ParseResponse, len(ma.respParsers))
		copy(parsers, ma.respParsers)
		return parsers
}

func appendDataList(dataList []module.Data, data module.Data, depth uint32) []module.Data {
	if data == nil {
		return dataList
	}
	req, ok := data.(*module.Request)
	if !ok {
		return append(dataList, data)
	}
	newDepth := depth + 1
	if req.Depth() != newDepth {
		req = module.NewRequest(req.HTTPReq(), newDepth)
	}
	return append(dataList, req)
}