/*
	定义了downloader， 去请求request， 把页面数据撞到Response里
*/
package downloader

import (
	"Gspider/module"
	"Gspider/module/stub"
	"net/http"
)

func New(
	mid module.MID,
	client *http.Client,
	scoreCalculator module.CalculateScore) (module.Downloader, error) {
		moduleBase, err := stub.NewModuleInternal(mid, scoreCalculator)
		if err != nil {
			return nil, err
		}
		if client == nil {
			return nil, genParamterError("client is nil")
		}
		return &myDownloader{
			ModuleInternal: moduleBase,
			httpClient: *client,
		}, nil
}


type myDownloader struct {
	// 方法提升
	stub.ModuleInternal
	httpClient http.Client
}

func (md *myDownloader) Download(req *module.Request) (*module.Response, error ) {
	md.ModuleInternal.IncrHandlingNumber()
	defer md.ModuleInternal.DecrHandlingNumber()
	if req == nil {
		return nil, genParamterError("nil http req")
	}
	httpReq := req.HTTPReq()
	if httpReq == nil {
		return nil, genParamterError("nil http request")
	}
	md.ModuleInternal.IncrAcceptedCount()
	httpResp, err := md.httpClient.Do(httpReq)
	if err != nil {
		return nil, err
	}
	md.ModuleInternal.IncrCompletedCount()
	return module.NewResponse(httpResp, req.Depth()), nil
}
