package pipeline

import (
	"Gspider/module"
	"Gspider/module/stub"
)

func New(
	mid module.MID,
	itemProcessors []module.ProcessItem,
	scoreCalculator module.CalculateScore) (module.Pipeline, error) {
		moduleBase, err := stub.NewModuleInternal(mid, scoreCalculator)
		if err != nil {
			return nil, err
		}
		if itemProcessors == nil{
			return nil, genParameterError("nil item processors list")
		}
		if len(itemProcessors) == 0 {
			return nil, genParameterError("empty item processors")
		}
		var innerProcessors []module.ProcessItem
		for _, item := range itemProcessors {
			if item == nil {
				err := genParameterError("nil item processor")
				return nil, err
			}
			innerProcessors = append(innerProcessors, item)
		}
		return &myPipeline{
			ModuleInternal: moduleBase,
			itemProcessors: itemProcessors,
		}, nil
}

type myPipeline struct {
	stub.ModuleInternal
	itemProcessors []module.ProcessItem
	failFast bool
}

func (mp *myPipeline) ItemProcessors() []module.ProcessItem {
	processors := make([]module.ProcessItem, len(mp.itemProcessors))
	copy(processors, mp.itemProcessors)
	return processors
}

func (mp *myPipeline) Send(item module.Item) []error {
	mp.ModuleInternal.IncrHandlingNumber()
	defer mp.ModuleInternal.DecrHandlingNumber()
	mp.ModuleInternal.IncrCalledCount()
	var errs []error
	if item == nil {
		errs = append(errs, genParameterError("nil item"))
		return errs
	}
	mp.ModuleInternal.IncrAcceptedCount()
	var currentItem = item
	for _, processor := range mp.itemProcessors {
		pItem, err := processor(currentItem)
		if err != nil {
			errs = append(errs, err)
			if mp.failFast {
				break
			}
		}
		if pItem != nil {
			currentItem = pItem
		}
	}
	if len(errs) == 0 {
		mp.ModuleInternal.IncrCompletedCount()
	}
	return errs
}

func (mp *myPipeline) FailFast() bool {
	return mp.failFast
}

func (mp *myPipeline) SetFailFast(failFast bool) {
	mp.failFast = failFast
}

type extraSummaryStruct struct {
	FailFast bool `json:"fail_fast"`
	ProcessorNumber int `json:"processor_number"`
}

func (mp *myPipeline) Summary() module.SummaryStruct {
	summary := mp.ModuleInternal.Summary()
	summary.Extra = extraSummaryStruct{
		FailFast: mp.failFast,
		ProcessorNumber: len(mp.itemProcessors),
	}
	return summary
}