/*
	定义计算分数的函数
*/

package module

type CalculateScore func(counts Counts) uint64


func CalculateScoreSimple(counts Counts) uint64 {
	return counts.CallCount +
		counts.AcceptCount<<1 +
		counts.CompleteCount<<2+
		counts.HandlingNumber<<4
}

func SetScore(module Module) bool {
	calculator := module.ScoreCalculator()
	if calculator == nil {
		calculator = CalculateScoreSimple
	}
	newScore := calculator(module.Counts())
	if newScore == module.Score() {
		return false
	}
	module.SetScore(newScore)
	return true
}
