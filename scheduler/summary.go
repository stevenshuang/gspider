package scheduler

import (
	"Gspider/module"
	"Gspider/toolkit/buffer"
	"encoding/json"
	"sort"
)

type SchedSummary interface {
	Struct() SummaryStruct
	String() string
}

func newSchedSummary(
	requestArgs RequestArgs,
	dataArgs DataArgs,
	moduleArgs ModuleArgs,
	sched *myScheduler) SchedSummary {
	if sched == nil {
		return nil
	}
	return &myScheSummary{
		requestArgs: requestArgs,
		dataArgs: dataArgs,
		moduleArgs: moduleArgs,
		sched: sched,
	}
}

type myScheSummary struct {
	requestArgs RequestArgs
	dataArgs    DataArgs
	moduleArgs  ModuleArgs
	maxDepth    uint32
	sched       *myScheduler
}

type SummaryStruct struct {
	RequestArgs     RequestArgs             `json:"request_args"`
	DataArgs        DataArgs                `json:"data_args"`
	ModuleArgs      ModuleArgsSummary       `json:"module_args"`
	Status          string                  `json:"status"`
	Downloaders     []module.SummaryStruct  `json:"downloaders"`
	Analyzers       []module.SummaryStruct  `json:"analyzers"`
	Pipelines       []module.SummaryStruct  `json:"pipelines"`
	ReqBufferPool   BufferPoolSummaryStruct `json:"req_buffer_pool"`
	RespBufferPool  BufferPoolSummaryStruct `json:"resp_buffer_pool"`
	ItemBufferPool  BufferPoolSummaryStruct `json:"item_buffer_pool"`
	ErrorBufferPool BufferPoolSummaryStruct `json:"error_buffer_pool"`
	NumURL          uint64                  `json:"num_url"`
}

func (one *SummaryStruct) Same(another SummaryStruct) bool {
	if !another.RequestArgs.Same(&one.RequestArgs) {
		return false
	}
	if another.DataArgs != one.DataArgs {
		return false
	}
	if another.ModuleArgs != one.ModuleArgs {
		return false
	}
	if another.Status != one.Status {
		return false
	}
	for i, ds := range another.Downloaders {
		if ds != one.Downloaders[i] {
			return false
		}
	}
	if another.Pipelines == nil || len(another.Pipelines) != len(one.Pipelines) {
		return false
	}
	for i, ps := range another.Pipelines {
		if ps != one.Pipelines[i] {
			return false
		}
	}
	if another.ReqBufferPool != one.ReqBufferPool {
		return false
	}
	if another.RespBufferPool != one.RespBufferPool {
		return false
	}
	if another.ItemBufferPool != one.ItemBufferPool {
		return false
	}
	if another.ErrorBufferPool != one.ErrorBufferPool {
		return false
	}
	if another.NumURL != one.NumURL {
		return false
	}
	return true
}

func (mss *myScheSummary) Struct() SummaryStruct {
	registrar := mss.sched.registrar
	return SummaryStruct{
		RequestArgs:     mss.requestArgs,
		DataArgs:        mss.dataArgs,
		ModuleArgs:      mss.moduleArgs.Summary(),
		Status:          GetStatusDescription(mss.sched.status),
		Downloaders:     getModuleSummaries(registrar, module.TYPE_DOWNLOADER),
		Analyzers:       getModuleSummaries(registrar, module.TYPE_ANALYZER),
		Pipelines:       getModuleSummaries(registrar, module.TYPE_PIPELINE),
		ReqBufferPool:   getBufferPoolSummary(mss.sched.reqBufferPool),
		RespBufferPool:  getBufferPoolSummary(mss.sched.respBufferPool),
		ItemBufferPool:  getBufferPoolSummary(mss.sched.itemBufferPool),
		ErrorBufferPool: getBufferPoolSummary(mss.sched.errBufferPool),
		NumURL:          mss.sched.urlMap.Len(),
	}
}


func (mss *myScheSummary) String() string {
	b, err := json.MarshalIndent(mss.Struct(), "", "    ")
	if err != nil {
		return ""
	}
	return string(b)
}

type BufferPoolSummaryStruct struct {
	BufferCap       uint32 `json:"buffer_cap"`
	MaxBufferNumber uint32 `json:"max_buffer_number"`
	BufferNumber    uint32 `json:"buffer_number"`
	Total           uint64 `json:"total"`
}

func getBufferPoolSummary(bufferPool buffer.Pool) BufferPoolSummaryStruct {
	return BufferPoolSummaryStruct{
		BufferCap:       bufferPool.BufferCap(),
		MaxBufferNumber: bufferPool.MaxBufferNumber(),
		BufferNumber:    bufferPool.BufferNumber(),
		Total:           bufferPool.Total(),
	}
}

func getModuleSummaries(registrar module.Registrar, mType module.Type) []module.SummaryStruct {
	moduleMap, _ := registrar.GetAllByType(mType)
	summaries := []module.SummaryStruct{}
	if len(moduleMap) > 0 {
		for _, module := range moduleMap {
			summaries = append(summaries, module.Summary())
		}
	}
	if len(summaries) > 1 {
		sort.Slice(summaries, func(i, j int) bool {
			return summaries[i].ID < summaries[j].ID
		})
	}
	return summaries
}
