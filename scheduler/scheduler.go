package scheduler

import (
	"Gspider/module"
	"Gspider/toolkit/buffer"
	"context"
	"errors"
	"fmt"
	"github.com/stevenshuang/cmap"
	"net/http"
	"strings"
	"sync"
	"time"
)

type Scheduler interface {
	Init(requestArgs RequestArgs, dataArgs DataArgs,
		moduleArgs ModuleArgs) (err error)
	Start(firstHttpReq *http.Request) (err error)
	StartWithReqList(httpReq []*http.Request) (err error)
	Stop() (err error)
	Status() Status
	ErrorChan() <-chan error
	Idle() bool
	Summary() SchedSummary
}

type myScheduler struct {
	maxDepth uint32
	acceptedDomainMap cmap.ConcurrentMap
	registrar module.Registrar
	reqBufferPool buffer.Pool
	respBufferPool buffer.Pool
	itemBufferPool buffer.Pool
	errBufferPool buffer.Pool
	urlMap cmap.ConcurrentMap
	ctx context.Context
	cancelFunc context.CancelFunc
	status Status
	statusLock sync.RWMutex
	summary SchedSummary
}

func NewScheduler() Scheduler {
	return &myScheduler{}
}

func (sched *myScheduler) Init(
	requestArgs RequestArgs, dataArgs DataArgs, moduleArgs ModuleArgs) (err error) {
	var oldStatus Status
	oldStatus, err = sched.checkAndSetStatus(SCHED_STATUS_INITIALIZING)
	if err != nil {
		return
	}
	defer func() {
		sched.statusLock.Lock()
		if err != nil {
			sched.status = oldStatus
		} else {
			sched.status = SCHED_STATUS_INITIALIZED
		}
		sched.statusLock.Unlock()
	}()
	if err = requestArgs.Check(); err != nil {
		return err
	}
	if err = dataArgs.Check(); err != nil {
		return err
	}
	if err = moduleArgs.Check(); err != nil {
		return err
	}
	if sched.registrar == nil {
		sched.registrar = module.NewRegistrar()
	} else {
		sched.registrar.Clear()
	}
	sched.maxDepth = requestArgs.MaxDepth
	sched.acceptedDomainMap, _ = cmap.NewConcurrentMap(1, nil)
	for _, domain := range requestArgs.AcceptedDomains {
		sched.acceptedDomainMap.Put(domain, struct {}{})
	}
	sched.urlMap, _ = cmap.NewConcurrentMap(16, nil)
	sched.initBufferPool(dataArgs)
	sched.resetContext()
	sched.summary = newSchedSummary(requestArgs, dataArgs, moduleArgs, sched)
	if err = sched.registerModules(moduleArgs); err != nil {
		return err
	}
	return nil
}

func (sched *myScheduler) StartWithReqList(httpReqList []*http.Request) (err error) {
	fmt.Println("Start with req List...")
	time.Sleep(2 * time.Second)
	defer func() {
		if p := recover(); p != nil {
			errMsg := fmt.Sprintf("fatal scheduler error: %s", p)
			err = genError(errMsg)
		}
	}()
	var oldStatus Status
	oldStatus, err = sched.checkAndSetStatus(SCHED_STATUS_STARTING)
	defer func() {
		sched.statusLock.Lock()
		if err != nil {
			sched.status = oldStatus
		} else {
			sched.status = SCHED_STATUS_STARTED
		}
		sched.statusLock.Unlock()
	}()
	if err != nil {
		return
	}
	if httpReqList == nil {
		err = fmt.Errorf("empty init request list")
		return
	}
	sched.download()
	sched.analyze()
	sched.pick()
	for index, req := range httpReqList {
		if req == nil {
			fmt.Printf("reqList[%d] == nil", index)
			continue
		}
		fmt.Printf("resList[%d] req is valid->", index)
		var primaryDomain string
		primaryDomain, err = getPrimaryDomain(req.Host)
		if err != nil {
			continue
		}
		fmt.Printf("reqList[%d] domain valid ", index)
		sched.acceptedDomainMap.Put(primaryDomain, struct {}{})
		if err = sched.checkBufferPoolForStart(); err != nil {
			return
		}
		fmt.Println("send req")
		sample := module.NewRequest(req, 0)
		sched.sendReq(sample)
		time.Sleep(3 * time.Second)
	}
	return
}

func (sched *myScheduler) Start(firstHttpReq *http.Request) (err error) {
	defer func() {
		if p := recover(); p != nil {
			errMsg := fmt.Sprintf("fatal scheduler error: %s", p)
			err = genError(errMsg)
		}
	}()
	var oldStatus Status
	oldStatus, err = sched.checkAndSetStatus(SCHED_STATUS_STARTING)
	defer func(){
		sched.statusLock.Lock()
		if err != nil {
			sched.status = oldStatus
		} else {
			sched.status = SCHED_STATUS_STARTED
		}
		sched.statusLock.Unlock()
	}()
	if err != nil {
		return
	}
	if firstHttpReq == nil {
		err = genParameterError("nil first http request")
		return
	}
	fmt.Println("the first request is valid")
	var primaryDomain string
	primaryDomain, err = getPrimaryDomain(firstHttpReq.Host)
	if err != nil {
		return
	}
	fmt.Println("primary domain ok")
	sched.acceptedDomainMap.Put(primaryDomain, struct {}{})
	if err = sched.checkBufferPoolForStart(); err != nil {
		return
	}
	sched.download()
	sched.analyze()
	sched.pick()
	fmt.Println("Scheduler started...")
	firstReq := module.NewRequest(firstHttpReq, 0)
	sched.sendReq(firstReq)
	return nil
}


func (sched *myScheduler) Stop() (err error) {
	fmt.Println("Stop scheduler ...")
	var oldStatus Status
	oldStatus, err = sched.checkAndSetStatus(SCHED_STATUS_STOPPING)
	defer func(){
		sched.statusLock.Lock()
		defer sched.statusLock.Unlock()
		if err != nil {
			sched.status = oldStatus
		} else {
			sched.status = SCHED_STATUS_STOPPED
		}
	}()
	if err != nil {
		return
	}
	sched.cancelFunc()
	sched.reqBufferPool.Close()
	sched.respBufferPool.Close()
	sched.itemBufferPool.Close()
	sched.errBufferPool.Close()
	fmt.Println("Scheduler has been closed")
	return nil
}

func (sched *myScheduler) ErrorChan() <-chan error {
	errBuffer := sched.errBufferPool
	errCh := make(chan error, errBuffer.BufferCap())
	go func(errBuffer buffer.Pool, errCh chan error) {
		for{
			if sched.canceled(){
				break
			}
			data, err := errBuffer.Get()
			if err != nil {
				fmt.Println("The error buffer closed")
				close(errCh)
				break
			}
			err, ok := data.(error)
			if !ok {
				errMsg := fmt.Sprintf("incorrent error type: %s", data)
				sendError(errors.New(errMsg), "", sched.errBufferPool)
				continue
			}
			if sched.canceled(){
				close(errCh)
				break
			}
			errCh <- err
		}
	}(errBuffer, errCh)
	return errCh
}

func (sched *myScheduler) Idle() bool {
	modleMap := sched.registrar.GetAll()
	for _, m := range modleMap {
		if m.HandlingNumber() > 0 {
			return false
		}
	}
	if sched.reqBufferPool.Total() > 0 ||
		sched.respBufferPool.Total() > 0 ||
		sched.itemBufferPool.Total() > 0 {
			return false
	}
	return true
}

func (sched *myScheduler) Status() Status {
	var status Status
	sched.statusLock.Lock()
	status = sched.status
	sched.statusLock.Unlock()
	return status
}
func (sched *myScheduler) Summary() SchedSummary {
	return sched.summary
}

func (sched *myScheduler) registerModules(moduleArgs ModuleArgs) error {
	for _, d := range moduleArgs.Downloaders {
		if d == nil {
			continue
		} else {
			ok, err := sched.registrar.Register(d)
			if err != nil {
				return genErrorByError(err)
			}
			if !ok {
				errMsg := fmt.Sprintf("Couldn't register downloader instance MID : %s", d.ID())
				return genError(errMsg)
			}
		}
	}
	for _, a := range moduleArgs.Analyzers {
		if a == nil {
			continue
		}
		ok, err := sched.registrar.Register(a)
		if err != nil {
			return genErrorByError(err)
		}
		if !ok {
			errMsg := fmt.Sprintf("Couldn't register analyzer MID: %s", a.ID())
			return genError(errMsg)
		}
	}
	for _, p := range moduleArgs.Pipelines {
		if p == nil {
			continue
		}
		ok, err := sched.registrar.Register(p)
		if err != nil {
			return genErrorByError(err)
		}
		if !ok {
			errMsg := fmt.Sprintf("Couldn't register pipeline MID: %s", p.ID())
			return genError(errMsg)
		}
	}
	return nil
}

func (sched *myScheduler) checkAndSetStatus(status Status) (oldStatus Status, err error) {
	sched.statusLock.Lock()
	defer sched.statusLock.Unlock()
	oldStatus = sched.status
	// 检查启动状态 正确的状态转换
	err = checkStatus(oldStatus, status, nil)
	if err != nil {
		sched.status = status
	}
	return
}

func (sched *myScheduler) initBufferPool(dataArgs DataArgs) {
	if sched.reqBufferPool != nil && !sched.reqBufferPool.Closed() {
		sched.reqBufferPool.Close()
	}
	sched.reqBufferPool, _ = buffer.NewPool(dataArgs.ReqBufferCap,
		dataArgs.ReqMaxBufferNumber)
	if sched.respBufferPool != nil && !sched.respBufferPool.Closed() {
		sched.respBufferPool.Close()
	}
	sched.respBufferPool, _ = buffer.NewPool(dataArgs.RespBufferCap,
		dataArgs.RespMaxBufferNumber)
	if sched.itemBufferPool != nil && !sched.itemBufferPool.Closed(){
		sched.itemBufferPool.Close()
	}
	sched.itemBufferPool, _ = buffer.NewPool(dataArgs.ItemBufferCap,
		dataArgs.ItemMaxBufferNumber)
	if sched.errBufferPool != nil && !sched.errBufferPool.Closed() {
		sched.errBufferPool.Close()
	}
	sched.errBufferPool, _ = buffer.NewPool(dataArgs.ErrorBufferCap,
		dataArgs.ErrorMaxBufferNumber)
 }

func (sched *myScheduler) resetContext() {
	sched.ctx, sched.cancelFunc = context.WithCancel(context.Background())
}

func (sched *myScheduler) canceled() bool {
	select {
	case <- sched.ctx.Done():
		return true
	default:
		return false
	}
}

func (sched *myScheduler) sendReq(req *module.Request) bool {
	// 终极谬表sched.reqBufferPool.Put(req)
	if req == nil {
		return false
	}
	if sched.canceled() {
		return false
	}
	httpReq := req.HTTPReq()
	if httpReq == nil {
		return false
	}
	reqUrl := httpReq.URL
	if reqUrl == nil {
		fmt.Println("the url is empty")
		return false
	}
	scheme := strings.ToLower(reqUrl.Scheme)
	if scheme != "http" && scheme != "https" {
		fmt.Println("scheme is not http or https")
		return false
	}
	if v := sched.urlMap.Get(reqUrl.String()); v != nil {
		fmt.Println("url repeat")
		return false
	}
	pd, _ :=getPrimaryDomain(httpReq.Host)
	if sched.acceptedDomainMap.Get(pd) == nil {
		if pd == "bing.net" {
			panic(httpReq.URL)
		}
		fmt.Println("host is ignore")
		return false
	}
	if req.Depth() > sched.maxDepth {
		fmt.Println("depth is too big ignore")
		return false
	}
	go func(req *module.Request) {
		if err := sched.reqBufferPool.Put(req); err != nil {
			fmt.Println("the request buffer is closed ", err)
		}
	}(req)
	sched.urlMap.Put(reqUrl.String(), struct {}{})
	return true
}
func sendResp(resp *module.Response, respBufferPool buffer.Pool) bool {
	if resp == nil || respBufferPool == nil || respBufferPool.Closed() {
		return false
	}
	go func(resp *module.Response) {
		if err := respBufferPool.Put(resp); err != nil {
			fmt.Println("the response buffer pool closed, Ignore put")
		}
	}(resp)
	return true
}

func sendItem(item module.Item, itemBufferPool buffer.Pool) bool {
	if item == nil || itemBufferPool == nil || itemBufferPool.Closed() {
		return false
	}
	go func(item module.Item){
		if err := itemBufferPool.Put(item); err != nil {
			fmt.Println("the item buffer pool closed, Ignore put")
		}
	}(item)
	return true
}

func (sched *myScheduler) download() {
	go func() {
		for {
			if sched.canceled() {
				break
			}
			data, err := sched.reqBufferPool.Get()
			if err != nil {
				fmt.Println("request buffer is closed.")
				break
			}
			req, ok := data.(*module.Request)
			if !ok {
				errMsg := fmt.Sprintf("incorrent request type: %T", req)
				sendError(errors.New(errMsg), "", sched.errBufferPool)
			}
			sched.downloadOne(req)
		}
	}()
}

func (sched *myScheduler) downloadOne(req *module.Request) {
	if req == nil {
		return
	}
	if sched.canceled() {
		return
	}
	// 拿一个下载器
	m, err := sched.registrar.Get(module.TYPE_DOWNLOADER)
	if err != nil || m == nil{
		//拿不出来， 或者是控制
		errMsg := fmt.Sprintf("couldn't get a downloader: %s", err)
		sendError(errors.New(errMsg), "", sched.errBufferPool)
		sched.sendReq(req)
		return
	}
	downloader, ok := m.(module.Downloader)
	if !ok {
		errMsg := fmt.Sprintf("incorrent downloader type: %T (MID %s)",
			m, m.ID())
		sendError(errors.New(errMsg), m.ID(), sched.errBufferPool)
		return
	}
	resp, err := downloader.Download(req)
	if resp != nil {
		sendResp(resp, sched.respBufferPool)
	}
	if err != nil {
		sendError(err, m.ID(), sched.errBufferPool)
	}
}

func (sched *myScheduler) analyze() {
	go func(){
		for {
			if sched.canceled() {
				break
			}
			data, err := sched.respBufferPool.Get()
			if err != nil {
				fmt.Println("the response pool buffer")
				break
			}
			resp, ok := data.(*module.Response)
			if !ok {
				errMsg := fmt.Sprintf("incorrent response type: %T", data)
				sendError(errors.New(errMsg), "", sched.errBufferPool)
			}
			sched.analyzeOne(resp)
		}
	}()
}

func (sched *myScheduler) analyzeOne(resp *module.Response) {
	if resp == nil {
		return
	}
	if sched.canceled() {
		return
	}
	m, err := sched.registrar.Get(module.TYPE_ANALYZER)
	if err != nil || m == nil {
		errMsg := fmt.Sprintf("couldn't get an analyzer %s", err)
		sendError(errors.New(errMsg), "", sched.errBufferPool)
		sendResp(resp, sched.respBufferPool)
		return
	}
	analyze, ok := m.(module.Analyzer)
	if !ok {
		errMsg := fmt.Sprintf("incorrent analyzer type: %T (MID %s)", m, m.ID())
		sendError(errors.New(errMsg), m.ID(), sched.errBufferPool)
		sendResp(resp, sched.respBufferPool)
	}
	dataList, errs := analyze.Analyze(resp)
	if dataList != nil {
		for _, data := range dataList {
			if data == nil {
				continue
			}
			switch d := data.(type) {
			case *module.Request:
				sched.sendReq(d)
			case module.Item:
				sendItem(d, sched.itemBufferPool)
			default:
				errMsg := fmt.Sprintf("Unsupported data type: %T", d)
				sendError(errors.New(errMsg), m.ID(), sched.errBufferPool)
			}
		}
	}
	if errs != nil {
		for _, err := range errs {
			sendError(err, m.ID(), sched.errBufferPool)
		}
	}
}

func (sched *myScheduler) pick() {
	go func(){
		for {
			if sched.canceled() {
				break
			}
			data, err := sched.itemBufferPool.Get()
			if err != nil {
				fmt.Println("the item pool closed")
				break
			}
			item, ok := data.(module.Item)
			if !ok {
				errMsg := fmt.Sprintf("incorrent item type: %T", data)
				sendError(errors.New(errMsg), "", sched.errBufferPool)
			}
			sched.pickOne(item)
		}
	}()
}

func (sched *myScheduler) pickOne(item module.Item) {
	if item == nil {
		return
	}
	if sched.canceled() {
		return
	}
	m, err := sched.registrar.Get(module.TYPE_PIPELINE)
	if err != nil || m == nil {
		errmsg := fmt.Sprintf("couldn't get a pipeline: %s", err)
		sendError(errors.New(errmsg), m.ID(), sched.errBufferPool)
		sendItem(item, sched.itemBufferPool)
		return
	}
	pipline, ok := m.(module.Pipeline)
	if !ok {
		errMsg := fmt.Sprintf("incorrect pipeline type: %T (MID: %s)",
			m, m.ID())
		sendError(errors.New(errMsg), m.ID(), sched.errBufferPool)
		sendItem(item, sched.itemBufferPool)
		return
	}
	errs := pipline.Send(item)
	if errs != nil {
		for _, err := range errs {
			sendError(err, m.ID(), sched.errBufferPool)
		}
	}
}

func (sched *myScheduler) checkBufferPoolForStart() error {
	if sched.reqBufferPool == nil {
		return genError("nil request buffer pool")
	}
	if sched.reqBufferPool != nil && sched.reqBufferPool.Closed() {
		sched.reqBufferPool, _ = buffer.NewPool(sched.reqBufferPool.BufferCap(),
			sched.reqBufferPool.MaxBufferNumber())
	}
	if sched.respBufferPool == nil {
		return genError("nil response buffer pool")
	}
	if sched.respBufferPool != nil && sched.respBufferPool.Closed() {
		sched.respBufferPool, _ = buffer.NewPool(
			sched.respBufferPool.BufferCap(), sched.respBufferPool.MaxBufferNumber())
	}
	// 检查条目缓冲池。
	if sched.itemBufferPool == nil {
		return genError("nil item buffer pool")
	}
	if sched.itemBufferPool != nil && sched.itemBufferPool.Closed() {
		sched.itemBufferPool, _ = buffer.NewPool(
			sched.itemBufferPool.BufferCap(), sched.itemBufferPool.MaxBufferNumber())
	}
	// 检查错误缓冲池。
	if sched.errBufferPool == nil {
		return genError("nil error buffer pool")
	}
	if sched.errBufferPool != nil && sched.errBufferPool.Closed() {
		sched.errBufferPool, _ = buffer.NewPool(
			sched.errBufferPool.BufferCap(), sched.errBufferPool.MaxBufferNumber())
	}
	return nil
}